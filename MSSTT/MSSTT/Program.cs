﻿using System;

namespace MSSTT
{
    class Program
    {
        private static SpeechService speechService;

        static void Main(string[] args)
        {
            speechService = new SpeechService();
            speechService.StartSpeechRecognition().Wait();
            Console.ReadLine();
        }
    }
}
