﻿using System;
using System.Threading.Tasks;
using Microsoft.CognitiveServices.Speech;

namespace MSSTT
{
    class SpeechService
    {
        private static SpeechRecognizer recognizer;

        private static string MicrosoftSpeechApiKey = "3b0ed5550d204f78add55476ea966aa5";
        private static string Speech_Region = "westeurope";

        private void CreateSpeechClient()
        {
            try
            {
                var speechConfig = SpeechConfig.FromSubscription(MicrosoftSpeechApiKey, Speech_Region);

                //instantiate new instance of speech recognizer and keeping it for the lifetime until the main window closes
                recognizer = new SpeechRecognizer(speechConfig);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An exception occured:{ex}");
            }
        }

        public async Task StartSpeechRecognition()
        {
            CreateSpeechClient();
            //wire up event handlers to speech events
            //recognizer.Recognizing += (sender, e) => RecognizingEventHandler(e);
            recognizer.Recognized += (sender, e) => RecognizedEventHandler(e);
            recognizer.Canceled += (sender, e) => CanceledEventHandler(e);
            recognizer.SessionStarted += (sender, e) => SessionStartedEventHandler(e);
            recognizer.SessionStopped += (sender, e) => SessionStoppedEventHandler(e);
            recognizer.SpeechStartDetected += (sender, e) => SpeechStartDetectedEventHandler(e);
            recognizer.SpeechEndDetected += (sender, e) => SpeechEndDetectedEventHandler(e);

            //start speech recognition
            await recognizer.StartContinuousRecognitionAsync();
        }

        public void StopSpeechRecognition()
        {
            //stop recognition
            recognizer.StopContinuousRecognitionAsync().Wait();

            //unsubscribe from events
            recognizer.Recognizing -= (sender, e) => RecognizingEventHandler(e);
            recognizer.Recognized -= (sender, e) => RecognizedEventHandler(e);
            recognizer.Canceled -= (sender, e) => CanceledEventHandler(e);
            recognizer.SessionStarted -= (sender, e) => SessionStartedEventHandler(e);
            recognizer.SessionStopped -= (sender, e) => SessionStoppedEventHandler(e);
            recognizer.SpeechStartDetected -= (sender, e) => SpeechStartDetectedEventHandler(e);
            recognizer.SpeechEndDetected -= (sender, e) => SpeechEndDetectedEventHandler(e);
        }

        #region Event Handlers
        private void RecognizingEventHandler(SpeechRecognitionEventArgs e)
        {
            Console.WriteLine($"\n    RecognizingEventHandler: {e.Result.Text}");
        }

        private void RecognizedEventHandler(SpeechRecognitionEventArgs e)
        {
            Console.WriteLine($"\n    RecognizedEventHandler: {e.Result.Text}");
        }

        private void CanceledEventHandler(SpeechRecognitionCanceledEventArgs e)
        {
            if (e.Reason == CancellationReason.Error)
            {
                Console.WriteLine($"Recognition Canceled. Reason: {e.Reason}, ErrorDetails: {e.ErrorDetails}");
            }
        }

        private void SessionStartedEventHandler(SessionEventArgs e)
        {
            Console.WriteLine("Session start detected.  Please start speaking.");
        }

        private void SessionStoppedEventHandler(SessionEventArgs e)
        {
            Console.WriteLine("Session stop detected.");
        }

        private void SpeechStartDetectedEventHandler(RecognitionEventArgs e)
        {
            Console.WriteLine("\n    Speech start detected.");
        }

        private void SpeechEndDetectedEventHandler(RecognitionEventArgs e)
        {
            Console.WriteLine("\n    Speech end detected.");
        }
        #endregion
    }
}
